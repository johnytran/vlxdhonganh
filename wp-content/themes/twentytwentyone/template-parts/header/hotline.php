<?php
/**
 * Displays the site navigation.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<div id="sidebar-primary" class="sidebar">
    <?php dynamic_sidebar( 'hotlinewidgets' ); ?>
</div>
