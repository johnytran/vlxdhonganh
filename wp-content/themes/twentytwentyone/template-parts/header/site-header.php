<?php
/**
 * Displays the site header.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

$wrapper_classes  = 'site-header';
$wrapper_classes .= has_custom_logo() ? ' has-logo' : '';
$wrapper_classes .= ( true === get_theme_mod( 'display_title_and_tagline', true ) ) ? ' has-title-and-tagline' : '';
$wrapper_classes .= has_nav_menu( 'primary' ) ? ' has-menu' : '';
?>

<header id="masthead" class="<?php echo esc_attr( $wrapper_classes ); ?>" role="banner">
	<div class="container-fluid">
	  <div class="row">
	    <div class="col-sm-5">
	      <?php get_template_part( 'template-parts/header/hotline' ); ?>
	    </div>
	    <div class="col-sm-2 d-flex justify-content-center">
	      <?php get_template_part( 'template-parts/header/logo' ); ?>
	    </div>
	    <div class="col-sm-5 d-flex justify-content-center main-menu">
	      <?php get_template_part( 'template-parts/header/site-nav' ); ?>
	    </div>
	  </div>
		<div class="row">
	    <div class="col-sm-12">
				<?php get_template_part( 'template-parts/header/site-branding' ); ?>
			</div>
		</div>
	</div>




</header><!-- #masthead -->
