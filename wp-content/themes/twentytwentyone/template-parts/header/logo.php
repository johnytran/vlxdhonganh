<?php
/**
 * Displays the site navigation.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<?php if ( has_custom_logo() && ! $show_title ) : ?>
  <div class="site-logo"><?php the_custom_logo(); ?></div>
<?php endif; ?>
