<?php
/**
 * Displays the site navigation.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<div class="container tools">
  <div class="row">
      <?php dynamic_sidebar( 'toolswidgets' ); ?>
  </div>
</div>
