=== XLTab - Accordions and Tabs for Elementor Page Builder ===
Contributors: webangon
Tags: elementor,tab,accordion,faq 
Requires at least: 4.0
Tested up to: 5.6
Requires PHP: 5.4
License: GPLv3
License URI: https://opensource.org/licenses/GPL-3.0

The XLTab plugin you install after Elementor! and enjoy ultimate tab accordion. 

== Description ==

Enhance your [Elementor](https://wordpress.org/plugins/elementor/) page building experience with custom tab, accordion.

### 6 FREE WIDGETS AND COUNTING

1. [Accordion](http://plugins.webangon.com/khara/xltab/?elementor_library=accordion) - Accordion in 12 styles. 
2. [Accordion Checkbox](http://plugins.webangon.com/khara/xltab/?elementor_library=accordion-checkbox) - Accordion-checkbox in 2 styles. 
3. [Image Accordion](http://plugins.webangon.com/khara/xltab/?elementor_library=image-accordion) - Image accordion 1 style.
4. [Tab](http://plugins.webangon.com/khara/xltab/?elementor_library=tab) - Tabs with 11 styles.
5. [Switch Tab](http://plugins.webangon.com/khara/xltab/?elementor_library=switch-tab) - Switch tabs with 3 styles.
6. [Vertical Tab](http://plugins.webangon.com/khara/xltab/?elementor_library=vertical-tab) - Vertical tab with 4 styles.

### 4 PRO WIDGETS AND COUNTING

1. [Faq-Accordion](http://plugins.webangon.com/khara/xltab/?elementor_library=accordion-faq) - Faq accordion 1 style. 
2. [App Tab](http://plugins.webangon.com/khara/xltab/?elementor_library=app-tab) - App tab in 3 styles. 
3. [Floating Tab](http://plugins.webangon.com/khara/xltab/?elementor_library=floating-tab) - Floating tab 1 style.
4. [Step Tab](http://plugins.webangon.com/khara/xltab/?elementor_library=tab-steps) - Step tabs with 4 styles.

### Buy Premium Version [Codecanyon](https://codecanyon.net/item/xltab-accordions-and-tabs-for-elementor-page-builder/24282243)

== Installation ==

Note : This plugin works with Elementor. Make sure you have [Elementor](https://wordpress.org/plugins/elementor/) installed.


1. Upload the plugin folder to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. You can type "XLTab" on your element tabs within Elementor editor and all the avialable elements will appear.

== Frequently Asked Questions ==

= Can I use the plugin without Elementor Page Builder? =

No. You cannot use without Elementor since it's an addon for Elementor.

= Does it work with any theme? =

Absolutely! It will work with any theme where Elementor works.

= What if I update to Premium version? =

Your existing elements/content will work with premium version. So you won't lose your developed contents.
