<?php
namespace Elementor;

if ( ! function_exists('xltab_insert_elementor') ){

	function xltab_insert_elementor($atts){
	    if(!class_exists('Elementor\Plugin')){
	        return '';
	    }
	    if(!isset($atts['id']) || empty($atts['id'])){
	        return '';
	    }

	    $post_id = $atts['id'];

	    $response = Plugin::instance()->frontend->get_builder_content_for_display($post_id);
	    return $response;
	}
 
}

add_shortcode('XLTAB_INSERT_TPL','Elementor\xltab_insert_elementor');


