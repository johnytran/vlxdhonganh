<?php
$align = $settings['tpos'];
switch ($settings['tmpl']) {
    case "one":
        $cls = 'xld-tab1 ';
        break;

    case "two":
        $cls = 'xld-tab2 ';
        break; 
        
    case "three":
        $cls = 'xld-tab3 ';
        break;

    case "four":
        $cls = 'xld-tab4 ';
        break;

    default:
}
$tabti = '';
$tabct = '';
foreach ($settings['tabs'] as $a){

    $icon = $a['icon']? '<i class="tbicon '.$a['icon'].'"></i>' : '';
    $title = $a['title']? '<span class="title">'.$a['title'].'</span>' : '';
    $sub = $a['sub']? '<p class="sub">'.$a['sub'].'</p>' : '';
    $tabti.= '<li><div class="tbinr"><a href="#">'.$icon.$title.$sub.'</a></div></li>';
    $tabct.= $this->icon_image($a);
}

 ?>

<div class="xldtab xlvtab1 <?php echo $cls.$align;?>">

    <ul class="tabs">
        <?php echo $tabti;?>
    </ul> <!-- / tabs -->
    <div class="tab_content">
       <?php echo $tabct;?>
    </div> 

</div> <!-- / tab -->


<style type="text/css">    

.xlvtab1 {
  position: relative;
  width: 100%;
  margin: 0 auto;
}
.xlvtab1 .tbinr{
  position: relative;
}
.xlvtab1 .tab_content{
    overflow: hidden;
    padding: 35px;
}
.xlvtab1 .tabs { 
  display: table;
  position: relative;
  margin: 0;
  padding: 0;
  width: 33.33%;
  float: left;
  list-style-type: none;
}

.xlvtab1 .tabs li {
  padding: 0;
  position: relative;
}
.xlvtab1 .tabs a {
  color: #888;
  display: block;
  outline: none;
  text-decoration: none;
  -webkit-transition: all 0.2s ease-in-out;
  -moz-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
  z-index: 1;
  
}

.xlvtab1 .tabs_item {
  display: none;
}

.xlvtab1 .tabs_item:first-child {
  display: block;
}

.xlvtab1.xld-tab1 li a{
    padding: 15px 20px 15px 40px;
    color: #303F9F;
    background: #f9f9f9;
    cursor: pointer;
    position: relative;
    vertical-align: middle;
    -webkit-transition: 1s all cubic-bezier(0.075, 0.82, 0.165, 1);
    transition: 1s all cubic-bezier(0.075, 0.82, 0.165, 1);  
  
}

.xlvtab1.xld-tab2 .tabs li a{
    width: 100%;
    border: 0;
    color: #8799a3;
    padding: 15px 30px;
}

/*1*/
.xlvtab1.xld-tab1 li:first-child a { 
  border-top-width: 0px !important;
}
/*4*/
.xlvtab1.xld-tab4.left .tabs{
    border-right: 2px solid #e7e7e7; 
}
.xlvtab1.xld-tab4.right .tabs{
    border-left: 2px solid #e7e7e7; 
}
.xlvtab1.xld-tab4.left .tabs li a:after{
    right: 0px;
}
.xlvtab1.xld-tab4.right .tabs li a:after{
    left: 0px;
}

.xlvtab1.xld-tab4.left .tabs li.current a:after {
    opacity: 1;
    right: -2px;
}
.xlvtab1.xld-tab4.right .tabs li.current a:after {
    opacity: 1;
    left: -2px;
}
</style>