<?php

$id = substr( $this->get_id_int(), 0, 3 );
$slider_options = [
    'id' => $id,
];

$align = ' '.$settings['inpalign'].' d-'.$id;
switch ($settings['tmpl']) {
    case "one":
        $cls = 'xld-tab1 ';
        break;

    case "two":
        $cls = 'xld-tab2 ';
        break; 
        
    case "three":
        $cls = 'xld-tab3 ';
        break;

    default:
}
$outtitle = '';
$tabct = '';

foreach ($settings['tabs'] as $a){

    $icon = $a['icon']? '<i class="tbicon '.$a['icon'].'"></i>' : '';
    $title = $a['title']? '<span>'.$a['title'].'</span>' : '';
    $outtitle .= $icon || $title? '<li><div class="inrtab"><div>'.$icon.$title.'</div></div></li>' : '';
    $tabct.= $this->icon_image($a);
}

 ?>

<?php echo '<div class="demoTab '.$settings['tmpl'].$align.'" data-xld =\''.wp_json_encode($slider_options).'\'>';?>         
    <ul class="resp-tabs-list">
        <?php echo $outtitle;?>
    </ul> 

    <div class="resp-tabs-container">                                                        
        <?php echo $tabct;?>
    </div>
</div> 


<style type="text/css">  

.demoTab.nvcenter ul.resp-tabs-list{
  margin:0px auto;
  text-align: center;
}

.demoTab.nvright ul.resp-tabs-list,.demoTab.nvleft ul.resp-tabs-list{
  width: 100%;
}

.demoTab.nvright ul.resp-tabs-list li{
  float: right;
}

.demoTab.nvcenter .resp-tabs-list li{
    float: none;
}
/*1*/
.demoTab.styleone .resp-tabs-list li.resp-tab-active .inrtab div:before {
    content: '';
    display: block;
    position: absolute;
    left: 0px;
    width: 100%;
    bottom: -2px;
    height: 2px;
    background: black;
}
.demoTab li.resp-tab-active .inrtab{
    /*margin-top: -12px;*/
    color: #444;
    background: white;
    position: relative;
}

.demoTab.styleone .resp-tabs-list li .inrtab {
    background: #7a81f4;
    color: #fff;
    font-weight: bold;
    transition: all 0.3s ease-in 0s;
}
/*2*/

.demoTab.styletwo .resp-tab-item{
  position:relative;
  margin:0px 10px;
}
.demoTab.styletwo .resp-tab-item span:before {
    content: "";
    width: 100%;
    height: 4px;
    background: #f6f6f6;
    position: absolute;
    bottom: 0;
    left: 0;
}
.demoTab.styletwo .resp-tab-item.resp-tab-active span:after {
    width: 100%;
    opacity: 1;
}
.demoTab.styletwo .resp-tab-item span:after {
    content: "";
    width: 0;
    height: 4px;
    background: #727cb6;
    position: absolute;
    bottom: 0;
    left: 0;
    opacity: 0;
    z-index: 1;
    transition: all 1s ease 0s;
}

/*3*/

.demoTab.stylethree .resp-tab-item span:after {
    content: "";
    width: 0;
    border-top: 3px solid #00a6ff;
    position: absolute;
    bottom: -1px;
    left: 0;
    transition: all 0.3s ease 0s;
    left: auto;
    right: 0;
}
 .demoTab.stylethree .resp-tab-item span:hover:after,.demoTab.stylethree .resp-tab-item.resp-tab-active span:after {
    width: 100%;
}
/*5*/
.demoTab.stylefive .resp-tab-item:last-child .inrtab:before{
    display:none;
}
.demoTab.stylefive .resp-tab-active .inrtab{
  background:#ff007a;
}
.demoTab.stylefive .resp-tab-item:last-child .inrtab:after,.demoTab.stylefive  .resp-tab-item:last-child .inrtab div:after {
  display:none;
}
.demoTab.stylefive .inrtab:before {
    border-bottom: 10px solid rgba(0, 0, 0, 0);
    border-left: 10px solid #fff;
    border-top: 10px solid rgba(0, 0, 0, 0);
    content: "";
    position: absolute;
    right: -8px;
    top: 11px;
    z-index: 2;
}
.demoTab.stylefive .resp-tab-active .inrtab:before {
    border-left: 10px solid #ff007a;
}
.demoTab.stylefive .resp-tab-active .inrtab:after,.demoTab.stylefive  .inrtab div:after {
    content: "";
    border-left: 10px solid #ff007a;
    border-top: 10px solid transparent;
    border-bottom: 10px solid transparent;
    position: absolute;
    top: 11px;
    right: -10px;
    z-index: 1;
}
.demoTab.stylefive .inrtab:after {
    border-left: 10px solid red;
}



</style>