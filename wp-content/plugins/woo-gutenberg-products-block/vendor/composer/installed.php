<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-trunk',
    'version' => 'dev-trunk',
    'aliases' => 
    array (
    ),
    'reference' => 'fbbe5857adc870276ea1783a4ea51dc6f94528ac',
    'name' => 'woocommerce/woocommerce-blocks',
  ),
  'versions' => 
  array (
    'automattic/jetpack-autoloader' => 
    array (
      'pretty_version' => 'v2.10.4',
      'version' => '2.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '70cb300a7a215ae87c671f600f77093518f87bac',
    ),
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae03311f45dfe194412081526be2e003960df74b',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'woocommerce/woocommerce-blocks' => 
    array (
      'pretty_version' => 'dev-trunk',
      'version' => 'dev-trunk',
      'aliases' => 
      array (
      ),
      'reference' => 'fbbe5857adc870276ea1783a4ea51dc6f94528ac',
    ),
  ),
);
